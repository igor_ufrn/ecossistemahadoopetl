#!/bin/sh
echo 'executando ENTRYPOINT com usuário:' $(whoami)
echo 'Iniciando SSH server'
/usr/sbin/sshd
if [ ! -d "/tmp/hadoop-root/dfs/name" ]; then
   #dfs.namenode.name.dir
   #dfs.datanode.data.dir
   echo 'Diretório para armazenamento de metadata do deamon namenode do hadoop: /tmp/hadoop-root/dfs/name'   
   echo 'Diretório para armazenamento de dados default dos datanodes do hadoop: /tmp/hadoop-root/dfs/data'   
   echo 'Formatando HDFS'
   hdfs namenode -format   
fi

echo 'Iniciando Deamons do Hadoop: Namenode e Datanode'
start-dfs.sh

hdfs dfs -test -d /codigoExemplos 
if [ $? -eq 0 ]; then
	echo 'Diretório com códigos de exemplos já estão no HDFS: /codigoExemplos' 
else
	echo 'Copiando os exemplos para o HDFS: /codigoExemplos e /dadosExemplos' 
   hdfs dfs -mkdir /codigoExemplos    
   hdfs dfs -mkdir /dadosExemplos 
   #hdfs dfs -put /codigoExemplos/*.* /codigoExemplos 
   #hdfs dfs -put /dadosExemplos/*.*  /dadosExemplos 
   hdfs dfs -put /codigoExemplos /
   hdfs dfs -put /dadosExemplos /
   hdfs dfs -put /exemplos /
   echo 'Os exemplos de streaming usam como pasta default a pasta no HDFS: /pastaMonitorada/' 
   hdfs dfs -mkdir /pastaMonitorada/
fi

#hdfs dfs -test -e /revisit/content/files/schema.xml
hdfs dfs -test -d /user
if [ $? -eq 0 ]; then
	echo 'Diretórios necessários para executar MAP REDUCE já existem: /user e /user/root'
else
	echo 'Criando diretórios necessários para executar MAP REDUCE: /user e /user/root'
   hdfs dfs -mkdir /user
   hdfs dfs -mkdir /user/root
fi
echo 'Iniciando Deamons do Hadoop: Namenode e Datanode'
start-dfs.sh
#Start ResourceManager daemon and NodeManager daemon
#ResourceManager - http://localhost:8088/
echo 'Iniciando daemons do YARN: ResourceManager e NodeManager'
start-yarn.sh

hdfs dfs -test -d /user/hive/warehouse
if [ $? -eq 0 ]; then
	echo 'Diretórios necessários para executar o HIVE já existem: /tmp e /user/hive/warehouse'
else
	echo 'Criando diretórios necessários para executar o HIVE: /tmp e /user/hive/warehouse'
   hdfs dfs -mkdir /tmp
   hdfs dfs -mkdir -p /user/hive/warehouse
   echo 'Configurando permissões nos diretórios necessários ao HIVE:'
   hdfs dfs -chmod g+w /tmp
   hdfs dfs -chmod g+w /user/hive/warehouse
fi



echo 'Iniciando configurações para funcionamento do DEAMON AEL do Pentaho'
FILE_PDI_SPARK_DRIVER=/opt/Pentaho/design-tools/data-integration/pdi-spark-driver.zip
if [ -f "$FILE_PDI_SPARK_DRIVER" ]; then
    echo "Não foi necessária criação do PDI_SPARK_DRIVER"
fi

echo 'Verificando se o Driver Spark do Pentaho já se encontra no HDFS...'
hdfs dfs -test -d /user/root/pentaho/driverAel/pdi-spark-executor.zip
if [ $? -eq 0 ]; then
	echo 'Driver Spark do Pentaho já se encontra no HDFS: /user/root/pentaho/driverAel/pdi-spark-driver.zip'
else
	echo 'Salvando Driver Spark do Pentaho no HDFS'
   hdfs dfs -mkdir -p /user/root/pentaho/driverAel
   hdfs dfs -put /opt/Pentaho/design-tools/pdi-spark-executor.zip /user/root/pentaho/driverAel
   #hdfs dfs -put /opt/Pentaho/design-tools/data-integration/pdi-spark-driver.zip /user/root/pentaho/driverAel   
   echo "PDI_SPARK_DRIVER salvo com sucesso no HDFS!"
fi

echo "Apagando CACHE do Karaf"
rm -Rf  /opt/Pentaho/design-tools/data-integration/system/karaf/caches/
mkdir -p /opt/Pentaho/design-tools/data-integration/system/karaf/caches/
echo "Iniciando DEAMON AEL..."
./$PENTAHO_HOME/design-tools/data-integration/adaptive-execution/daemon.sh start
#echo "Processo de inicialização do DEAMON AEL finalizado. A seguir, últimas linhas do LOG de inicialização:"
#tail   /opt/Pentaho/design-tools/data-integration/adaptive-execution/logs/daemon.log 
echo "Processo de inicialização do DEAMON AEL finalizado"


echo 'Criando Metastore do Hive...'
schematool -dbType postgres -initSchema
if [ $? -eq 0 ]; then
   echo 'Metastore do Hive criado com sucesso!'   
else
   echo 'WARN: Não foi criado Metastore do HIVE: Ele talvez já exista'
fi
echo 'Iniciando HIVE SERVER'   
hiveserver2 &

echo 'Iniciando SPARK MASTER'   
start-master.sh --host 0.0.0.0 --port 7077 --webui-port 8080
echo 'Iniciando SPARK SLAVE'   
start-slave.sh spark://hadoopmaster:7077

## INICIO Configurações do Jupyter ##
if [ ! -d "/notebookDir" ]; then
   echo 'Criando diretório para o jupyter notebook: /notebookDir'
   mkdir /notebookDir 
else
   echo 'Utilizando o seguinte diretório para o jupyter notebook: /notebookDir'
fi
echo 'Iniciando o servidor do Jupyter'
jupyter notebook -y  --port=8888 --no-browser --ip=0.0.0.0 --allow-root  --NotebookApp.token='' --NotebookApp.password='' --notebook-dir /notebookDir &
## FIM Configurações do Jupyter ##
#Permitir pseudo terminal

#echo 'Esperando um pouco para que os processos (DEAMONS) iniciem a escuta nas portas do sistema...'
#sleep 20
echo 'Processos estão escutando nas seguintes portas:'
lsof -i -P -n | grep LISTEN
echo 'Quais deamons do Hadoop estão executando?'
jps

/bin/sh