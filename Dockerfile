FROM redhat/ubi8:8.5-226.1645809065

ENV AWS_ACCESS_KEY_ID "test"
ENV AWS_SECRET_ACCESS_KEY "test"
ENV AWS_DEFAULT_REGION "sa-east-1"

RUN  mkdir /exemplos
COPY exemplos/ /exemplos/

RUN yum update -y
RUN yum install -y https://dl.fedoraproject.org/pub/epel/epel-release-latest-8.noarch.rpm
#RUN yum install -y pdsh
RUN yum install -y xz-devel.x86_64 python3-sqlalchemy.x86_64  java-1.8.0-openjdk-devel libsqlite3x-devel.x86_64 gcc gcc-c++ openssl-devel yum-utils make bzip2-devel libffi-devel  wget cyrus-sasl-devel.x86_64 cyrus-sasl-plain.x86_64  tar zip  unzip passwd hostname  procps iputils nc nano net-tools lsof  openssl openssh-server openssh-clients htop 
#python36-devel.x86_64
RUN curl https://www.python.org/ftp/python/3.7.13/Python-3.7.13.tgz -o "/opt/Python-3.7.13.tgz"
RUN tar xzf /opt/Python-3.7.13.tgz -C /opt
RUN ./opt/Python-3.7.13/configure --enable-optimizations
RUN make altinstall
RUN rm /opt/Python-3.7.13.tgz
RUN pip3.7 install --upgrade pip
RUN pip3.7 install sasl thrift thrift_sasl   pyhive avro numpy pymongo pandas jupyter findspark boto3 kafka-python
RUN alternatives --remove python /usr/libexec/no-python
RUN alternatives --install /usr/bin/python python /usr/local/bin/python3.7 1 
#RUN pip3.6 install --upgrade pip
#RUN pip3.6 install sasl sqlalchemy thrift thrift_sasl   pyhive avro numpy pymongo pandas jupyter findspark boto3  kafka-python
RUN  mkdir /entrypoint
COPY entrypoint.sh /entrypoint

RUN mkdir -p /opt/Pentaho/design-tools/
COPY pdi-spark-driver.zip /tmp/pdi.zip
RUN  unzip -q /tmp/pdi.zip -d /opt/Pentaho/design-tools/
RUN  mv  /tmp/pdi.zip /opt/Pentaho/design-tools/data-integration/pdi-spark-driver.zip
RUN find /opt/Pentaho/ -type f -iname "*.sh" -exec chmod +x {} \;


RUN mkdir -p /exemplos/dados/hive
COPY exemplos/dados/hive /exemplos/dados/hive

RUN  mkdir /opt/hadoop
RUN  mkdir /opt/hive

#RUN alternatives --set python /usr/bin/python3.9
#RUN yum  install -y wget yum-utils make gcc openssl-devel bzip2-devel libffi-devel zlib-devel
#RUN curl  "https://www.python.org/ftp/python/3.9.9/Python-3.9.9.tgz" -o "Python-3.9.9.tgz"
#RUN tar xzf Python-3.9.9.tgz
#RUN cd Python-3.9.9 && ./configure --with-system-ffi --with-computed-gotos --enable-loadable-sqlite-extensions
#RUN cd Python-3.9.9 && make -j ${nproc}
#RUN cd Python-3.9.9 && make altinstall
#RUN python3.9 -m pip install --upgrade pip
#RUN pip3.9 install numpy pandas jupyter findspark koalas
#RUN rm Python-3.9.9.tgz




#COPY pdi-ce-9.2.0.0-290.zip /tmp/pdi.zip
#RUN  unzip -q /tmp/pdi.zip -d /opt
#RUN  rm -rf  /tmp/pdi.zip
#RUN find /opt/Pentaho/ -type f -iname "*.sh" -exec chmod +x {} \;




COPY hadoop-3.3.2.tar.gz /tmp/hadoop.tar.gz
RUN  tar -xf /tmp/hadoop.tar.gz -C /opt/hadoop
RUN  rm -rf /tmp/hadoop.tar.gz

COPY apache-hive-3.1.2-bin.tar.gz /tmp/hive.tar.gz
RUN  tar -xf /tmp/hive.tar.gz -C /opt/hive
RUN  rm -rf  /tmp/hive.tar.gz




###SPARK###
RUN  mkdir /opt/spark
#RUN curl -o /tmp/spark.tgz https://dlcdn.apache.org/spark/spark-3.2.1/spark-2.4.0-bin-hadoop2.7.tgz
#COPY spark-2.4.0-bin-hadoop2.7.tgz /tmp/spark.tgz
#RUN  tar -xf /tmp/spark.tgz -C /opt/spark
#RUN  rm -rf /tmp/spark.tgz


COPY spark-2.4.0-bin-hadoop2.7.tgz /tmp/spark.tgz
RUN  tar -xf /tmp/spark.tgz -C /opt/spark
RUN  rm -rf /tmp/spark.tgz

#RUN curl -o /opt/spark/spark-2.4.0-bin-hadoop2.7/jars/postgresql-42.3.3.jar https://jdbc.postgresql.org/download/postgresql-42.3.3.jar
COPY postgresql-42.3.3.jar /opt/spark/spark-2.4.0-bin-hadoop2.7/jars/postgresql-42.3.3.jar
COPY postgresql-42.3.3.jar /opt/hive/apache-hive-3.1.2-bin/lib/postgresql-42.3.3.jar
COPY mysql-connector-java-8.0.28.jar /opt/spark/spark-2.4.0-bin-hadoop2.7/jars/mysql-connector-java-8.0.28.jar
#COPY mongo-spark-connector_2.12-3.0.1.jar /opt/spark/spark-2.4.0-bin-hadoop2.7/jars/mongo-spark-connector_2.12-3.0.1.jar
#COPY mongo-java-driver-3.12.10.jar /opt/spark/spark-2.4.0-bin-hadoop2.7/jars/mongo-java-driver-3.12.10.jar
RUN  mkdir /dadosExemplos
COPY dadosExemplos/ /dadosExemplos/
RUN  mkdir /codigoExemplos





COPY init/codigo/exemplosSpark/ /codigoExemplos/
ENV SPARK_HOME "/opt/spark/spark-2.4.0-bin-hadoop2.7"
ENV PATH "${SPARK_HOME}/bin:${SPARK_HOME}/sbin:${PATH}"
###SPARK###





#Permitir que seja realizado SSH pelo usuário root - WinSCP
RUN ssh-keygen -t ecdsa -f /etc/ssh/ssh_host_ed25519_key
RUN ssh-keygen -t rsa -f /etc/ssh/ssh_host_rsa_key
RUN ssh-keygen -t dsa -f /etc/ssh/ssh_host_dsa_key
RUN ssh-keygen -t ecdsa -f /etc/ssh/ssh_host_ecdsa_key
RUN echo 'root' | passwd --stdin root

#Hadoop can also be run on a single-node in a pseudo-distributed mode where each Hadoop daemon runs in a separate Java process
#Permitir o SSH para localhost sem senha: ssh localhost
RUN ssh-keygen -t rsa -P '' -f ~/.ssh/id_rsa
RUN cat ~/.ssh/id_rsa.pub >> ~/.ssh/authorized_keys
RUN chmod 0600 ~/.ssh/authorized_keys


#Configuração do SSH para não conferir a autenticidade do HOST
RUN echo 'Host *' >> /etc/ssh/ssh_config
RUN echo '  StrictHostKeyChecking no'     >> /etc/ssh/ssh_config
RUN echo '  UserKnownHostsFile=/dev/null' >> /etc/ssh/ssh_config


RUN HDFS_NAMENODE_USER="root"
RUN HDFS_DATANODE_USER="root"
RUN HDFS_SECONDARYNAMENODE_USER="root"
RUN YARN_RESOURCEMANAGER_USER="root"
RUN YARN_NODEMANAGER_USER="root"

ENV HADOOP_HOME "/opt/hadoop/hadoop-3.3.2"
ENV HADOOP_CONF_DIR="/opt/hadoop/hadoop-3.3.2/etc/hadoop/"
ENV JAVA_HOME "/usr/lib/jvm/java-1.8.0-openjdk-1.8.0.332.b09-1.el8_5.x86_64"
#RUN echo 'export JAVA_HOME=$(dirname $(dirname $(readlink -f $(which javac))))' >> /etc/profile.d/global_env.sh
ENV HIVE_HOME "/opt/hive/apache-hive-3.1.2-bin"
ENV PATH "${HADOOP_HOME}/bin:${HADOOP_HOME}/sbin:${JAVA_HOME}:${HIVE_HOME}/bin:${PATH}"

ENV PENTAHO_HOME "/opt/Pentaho"
ENV PENTAHO_INSTALLED_LICENSE_PATH "/opt/Pentaho/installedLicenses.xml"
ENV PENTAHO_JAVA_HOME "/usr/lib/jvm/java-1.8.0-openjdk-1.8.0.332.b09-1.el8_5.x86_64"
ENV LD_LIBRARY_PATH=/opt/hadoop/hadoop-3.3.2/lib/native:$LD_LIBRARY_PATH


RUN wget https://repo1.maven.org/maven2/org/apache/spark/spark-avro_2.11/2.4.0/spark-avro_2.11-2.4.0.jar -P $SPARK_HOME/jars/
#RUN echo spark.driver.extraClassPath $SPARK_HOME/jars/spark-avro_2.11-2.4.0.jar >>  $SPARK_HOME/conf/spark-defaults.conf
#RUN echo spark.executor.extraClassPath $SPARK_HOME/jars/spark-avro_2.11-2.4.0.jar >>  $SPARK_HOME/conf/spark-defaults.conf


ENTRYPOINT ["/entrypoint/entrypoint.sh"]

