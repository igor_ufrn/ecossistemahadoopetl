#!/bin/sh
FILES=/docker-entrypoint-initdb.d/collections/*

for f in $FILES; do    
    mongoimport --db mongodb --legacy --file $f
done