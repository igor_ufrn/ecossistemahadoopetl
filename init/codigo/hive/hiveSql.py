import pandas
from pyhive import hive
conn = hive.Connection(host="hadoopmaster", port=10000, database="default")
cursor = conn.cursor()

cursor.execute("CREATE EXTERNAL TABLE IF NOT EXISTS usuario (login STRING, senha STRING, nome STRING, perfil STRING, situacao STRING, ts_operacao TIMESTAMP)")
cursor.execute("insert into usuario (login, senha, nome, perfil, situacao, ts_operacao) VALUES ('igorManual','password','Igor Linnik', 'ADMIN', 'ATIVO', '2021-01-01 00:00:00')")

cursor.execute("SELECT * FROM usuario")
for result in cursor.fetchall():
  print(result)


print('-------------------------------')
print('----------PANDAS---------------')
print('-------------------------------')
df = pandas.read_sql("SELECT * FROM usuario", conn)
print(df)
print('-------------------------------')
print('-------------------------------')
