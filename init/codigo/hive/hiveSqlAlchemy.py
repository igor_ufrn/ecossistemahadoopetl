import pandas
from sqlalchemy import create_engine
engine = create_engine("hive://hadoopmaster:10000/default")
engine.execute("CREATE EXTERNAL TABLE IF NOT EXISTS usuario (login STRING, senha STRING, nome STRING, perfil STRING, situacao STRING, ts_operacao TIMESTAMP)")
engine.execute("insert into usuario (login, senha, nome, perfil, situacao, ts_operacao) VALUES ('igorManual','password','Igor Linnik', 'ADMIN', 'ATIVO', '2021-01-01 00:00:00')")
df = pandas.read_sql("SELECT * FROM usuario", engine)
print(df)