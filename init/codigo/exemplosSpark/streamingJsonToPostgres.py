
from pyspark.sql import SparkSession


def atualizarBancoPostgres(df,batchId):
    df.write.format("jdbc").mode('append').option("url","jdbc:postgresql://postgres:5432/postgres").option("dbtable","postsstreaming").option("user","usuario").option("password","senha").option("driver","org.postgresql.Driver").save()


if __name__ == '__main__':
    spark = SparkSession.builder.appName("streamingJsonToConsole").getOrCreate()
    esquemaJson = "nome STRING, postagem STRING, data INT"
    df = spark.readStream.json("/pastaMonitorada/", schema=esquemaJson)
    diretorioStateSessionApp = "/tmp/streamingJsonToPostgres"    
    stcall = df.writeStream.foreachBatch(atualizarBancoPostgres).outputMode("append").trigger(processingTime="5 second").option("checkpointlocation",diretorioStateSessionApp).start()
    stcall.awaitTermination()

    
