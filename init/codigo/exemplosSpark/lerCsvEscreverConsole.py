from pyspark.sql import SparkSession
from pyspark.sql.functions import *

if __name__ == '__main__':
    spark = SparkSession.builder.appName("App").getOrCreate()
    esquemaArquivo = "id INT, nome STRING, status STRING, cidade STRING, vendas INT, data STRING"
    dfArquivo = spark.read.csv("/dadosExemplos/despachantes.csv", header=False, schema=esquemaArquivo)
    vendasPorAno = dfArquivo.select("data").groupBy(year("data")).count()
    vendasPorAno.write.format("console").save()
    spark.stop()


