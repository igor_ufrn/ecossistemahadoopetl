import findspark
findspark.init()
from pyspark.sql import SparkSession
spark = SparkSession.builder.appName("readUsersParquetInHDFS").getOrCreate()
dfparquet = spark.read.format("parquet").load("hdfs://hadoopmaster:9000/tmp/usuarios.parquet")
dfparquet.show()
spark.stop()