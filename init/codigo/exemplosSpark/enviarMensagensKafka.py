import findspark
findspark.init()
import pyspark
from pyspark.sql import SparkSession
import boto3
import json
import sys

from kafka import KafkaProducer
from kafka import KafkaConsumer
import time

value = '{"login":"user000","nome":"Igor Linnik","perfil":"ADMIN","senha":"password","situacao":"ATIVO","ts_operacao":"2021-01-01"}'
producer   = KafkaProducer(bootstrap_servers=['kafka:9092'],value_serializer=lambda x: str.encode(x), key_serializer=lambda x: str.encode(x))

i = 0
while(i < 10):
  producer.send('usuarios', value=value, key='ESTA CHAVE FAZ COM QUE UM MEMBRO DO GRUPO SEJA ATRIBUIDO A UMA PARTICAO')
  print("------------------------------------------------")
  print("MENSAGEM ENVIADA PARA O KAFKA")
  print("------------------------------------------------")  
  print(str(value))
  i = i + 1


#spark.stop()