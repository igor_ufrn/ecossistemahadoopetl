import findspark
findspark.init()
from pyspark.sql import SparkSession
spark = SparkSession.builder.appName("validacaoCluster").getOrCreate()
sc = spark.sparkContext


numeros = sc.parallelize([1,2,3,4,5,6,7,8,9,10])
print(numeros.top(5))
print(numeros.collect())
print(numeros.count())
print(numeros.mean())
print(numeros.sum())
print(numeros.max())
print(numeros.min())
print(numeros.stdev())

consulta  = numeros.filter(lambda num: num < 5)
consulta.collect()

dobro  = numeros.map(lambda num: num * 2)
dobro.collect()

numeros_aux =sc.parallelize([11,12,13,14,15])
produtoCartesiano = numeros.cartesian(numeros_aux)
produtoCartesiano.collect()

uniao = numeros.union(numeros_aux)
uniao.collect()


df = spark.createDataFrame([("A",2),("B",4),("C",11)])
df.show()


schema = "Campo STRING, Valor INT"
data = ([("A",2),("B",4),("C",11),("D",99)])
df2 = spark.createDataFrame(data,schema)
df2.show()
df2.show(1)

from pyspark.sql.functions import sum
df2.groupBy("Campo").agg(sum("Valor")).show()
df2.select("Campo").show()
df2.select("Valor","Campo").show()

from pyspark.sql.functions import expr
df2.select("Campo","Valor",expr("Valor * 0.5")).show()
df2.schema
df2.columns

from pyspark.sql.types import *
esquemaArquivo = "id INT, nome STRING, status STRING, cidade STRING, vendas INT, data STRING"
dfArquivo = spark.read.csv("/dadosExemplos/despachantes.csv",header=False, schema=esquemaArquivo)
dfArquivo.show()

dfArquivoEsquemaAut = spark.read.load("/dadosExemplos/despachantes.csv", header=False, format="csv", sep=",", inferSchema=True)
dfArquivoEsquemaAut.show()
dfArquivoEsquemaAut.schema

dfArquivo.write.mode("overwrite").format("parquet").save("/resultados/parquet")
dfArquivo.write.mode("overwrite").format("csv").save("/resultados/csv")
dfArquivo.write.mode("overwrite").format("json").save("/resultados/json")
dfArquivo.write.mode("overwrite").format("orc").save("/resultados/orc")

df = spark.read.format("parquet").load("/resultados/parquet")
df = spark.read.format("csv").load("/resultados/csv")
df = spark.read.format("json").load("/resultados/json")
df = spark.read.format("csv").load("/resultados/csv")
df.show()
print("--------------------------------------------------")
print("--------------------------------------------------")
print("Fim validação básica do Ecossistema Hadoop e Spark")
print("--------------------------------------------------")
print("--------------------------------------------------")
spark.stop()