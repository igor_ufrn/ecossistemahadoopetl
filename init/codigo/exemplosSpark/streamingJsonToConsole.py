from pyspark.sql import SparkSession


if __name__ == '__main__':
    spark = SparkSession.builder.appName("streamingJsonToConsole").getOrCreate()
    esquemaJson = "nome STRING, postagem STRING, data INT"
    df = spark.readStream.json("/pastaMonitorada/", schema=esquemaJson)
    diretorioStateSessionApp = "/tmp/streamingJsonToConsole"
    stcall = df.writeStream.format("console").outputMode("append").trigger(processingTime="5 second").option("checkpointlocation",diretorioStateSessionApp).start()
    stcall.awaitTermination()

    
