import findspark
findspark.init()
import pyspark
#Se há muito código pandas, poderia usar koalas para uma migração rápida - Questões de desempenho poderão ser tratadas aos poucos, mas a migração é rápida.
import pandas
from pyspark.sql import SparkSession

dfPandas = pandas.read_csv("/dadosExemplos/Churn.csv",sep=";")
spark = SparkSession.builder.appName("pandasApp").getOrCreate()
pandasToDataFrameSpark = spark.createDataFrame(dfPandas)

print("DATAFRAME SPARK")
pandasToDataFrameSpark.show(5)

print("DATAFRAME PANDAS")
dfPandasByDfSpark = pandasToDataFrameSpark.toPandas()
dfPandasByDfSpark.head(5)