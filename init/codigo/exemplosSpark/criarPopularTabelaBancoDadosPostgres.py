import sys, getopt
from pyspark.sql import SparkSession
from pyspark.sql.functions import *

if __name__ == '__main__':
    spark = SparkSession.builder.appName("App").getOrCreate()
    opts, args = getopt.getopt(sys.argv[1:],"a:t:")
    arquivo, tabela = "",""
    for opt, arg in opts:
        if opt == '-a':
            arquivo = arg
        elif opt == "-t":
            tabela = arg
    df=spark.read.load(arquivo)
    df.write.format("jdbc").option("url","jdbc:postgresql://postgres:5432/postgres").option("dbtable",tabela).option("user","usuario").option("password","senha").option("driver","org.postgresql.Driver").save()
    spark.stop()


