import findspark
findspark.init()
import pyspark
from pyspark.sql import SparkSession
import boto3
import json
import sys

from kafka import KafkaProducer
from kafka import KafkaConsumer
import time

BUCKET_NAME = 'bucketname'
KEY = 'arquivoJson.json'
OUTPUT = 'arquivoJson.json'
s3 = boto3.resource('s3', endpoint_url='http://localstack:4566')
s3.Bucket(BUCKET_NAME).download_file(KEY, OUTPUT)
with open(OUTPUT) as f: 
  dicionario=json.load(f)
  jsonObj = json.dumps(dicionario)


spark = SparkSession.builder.appName("app").getOrCreate()
dfArquivo = spark.read.json("file:///notebookDir/arquivoJson.json", multiLine=True)

dfArquivo.show()
dfArquivo.write.mode("append").format("jdbc").option("url","jdbc:mysql://mysql:3306/mysql").option("dbtable","usuario").option("user","usuario").option("password","senha").option("driver","com.mysql.cj.jdbc.Driver").save()

producer = KafkaProducer(bootstrap_servers=['kafka:9092'],value_serializer=lambda x: json.dumps(x).encode('utf-8'))
producer.send('usuarios', value=dicionario)
print("------------------------------------------------")
print("MENSAGEM ENVIADA PARA O KAFKA")
print("------------------------------------------------")  
print(str(dicionario))

consumer = KafkaConsumer(
    'usuarios',
     consumer_timeout_ms=4000,
     enable_auto_commit=True,
     bootstrap_servers=['kafka:9092'],     
     group_id='servicoETL',
     value_deserializer=lambda x: json.loads(x.decode('utf-8')))

for message in consumer:
  message = message.value    
  print("------------------------------------------------")
  print("MENSAGEM RECEBIDA DO KAFKA")
  print("------------------------------------------------")  
  print(str(message))

consumer.close()
spark.stop()