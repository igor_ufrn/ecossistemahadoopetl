CREATE TABLE IF NOT EXISTS `usuario` (
  `login` text NOT NULL,
  `senha` text NOT NULL,
  `nome` text NOT NULL,
  `perfil` text NOT NULL,
  `situacao` text NOT NULL,
  `ts_operacao` timestamp NOT NULL
) ENGINE='InnoDB';