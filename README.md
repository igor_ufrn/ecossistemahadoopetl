Este repositório reproduz em ambiente local (máquina do dosenvolvedor) toda uma 
infraestrutura de big data que geralmente é disponibilizada por uma instituição.

- Hadoop e uso de seus componentes: YARN, HDFS, etc
- Diversos bancos de dados relacionais e não relacionais
- Uso de serviço AWS de forma OFFLINE através de Localstack
- Sistema de mensageria Kafka
- Spark
- Jupyter
- Hive
- Pentaho PDI em container
- Integração do Pentaho com todas as tecnologias citadas acima
- Uso da camada de execução adaptativa do Pentaho para executar
- JOBS através de aplicação Spark dentro do componente YARN do Hadoop.
- Diversos exemplos de aplicações Spark e JOBS do Pentaho.
